import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

void main(){
  return runApp(MyApp());
}

class MyApp extends StatelessWidget {


  Widget profileView = Container(
    child: Row(
      children: [
        Expanded(
          flex: 1,
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              child: 
              Image.asset('image/profile.jpg',
              width: 130,height: 200,fit: BoxFit.cover,)
            )
          ]
        )),
        Expanded(
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
             Text("ประวัติส่วนตัว:",style: TextStyle(fontSize: 25)),
            Text("ชื่อ:อิสรัตน์ ใจจรูญ",style: TextStyle(fontSize: 20)),
            Text("ชื่อเล่น:อาร์ม",style: TextStyle(fontSize: 20)),
            Text("วันเกิด:21/08/1999",style: TextStyle(fontSize: 20)),
            Text("เพศ:ชาย",style: TextStyle(fontSize: 20)),
          ],
        ))
      ],
    ),
  );
  
  Widget educationView = Container(
    
    child: Card(
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.import_contacts_outlined,color: Colors.black),
                    title: const Text('ประวัติการศึกษา',style: TextStyle(fontSize: 25),)
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text(
                      '2015-2018 ระดับมัธยมโรงเรียนบ้านบึง "อุตสาหกรรมนุเคราะห์" \n'
                      '2018-ปัจจุบัน ระดับอุดมศึกษามหาวิทยาลัยบูรพาคณะวิทยาการสารสนเทศ สาขาวิทยาการคอมพิวเตอร์',
                      style: TextStyle(fontSize: 20),
                    ),
      )])));

  Widget experienceView = Container(
    
    child: Card(
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.all_inbox,color: Colors.black),
                    title: const Text('ประสบการณ์',style: TextStyle(fontSize: 25),)
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text(
                      '2020 TeamProject UserInterface Designand Development โปรแกรมขายสินค้าร้านinthanincoffe'
                      '\n2020 TeamProject Software Development โปรแกรมขายสินค้าหน้าร้าน'
                      '\n2020-2021 TeamProject Web Programming เว็บสมัคร/หางาน'
                      '\n2021 Project Mobile Application Development I เกม Hangman'
                      ,style: TextStyle(fontSize: 20),
                    ),
      )])));
  
  Widget languageView = Container(
    
    child: Card(
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.import_contacts,color: Colors.black),
                    title: const Text('ภาษา',style: TextStyle(fontSize: 25),)
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Wrap(
                      direction: Axis.horizontal,
                      runSpacing: 20.0,
                      children: [
                        Image.network('https://img-premium.flaticon.com/png/512/644/premium/644609.png?token=exp=1627198787~hmac=7487909e1f81e25a1a090748e036ffc5',width: 75,height: 75),
                        Image.network('https://image.flaticon.com/icons/png/512/1199/1199124.png',width: 75,height: 75),
                        Image.network('https://image.flaticon.com/icons/png/512/174/174854.png',width: 75,height: 75),
                        Image.network('https://image.flaticon.com/icons/png/512/732/732190.png',width: 75,height: 75),
                        Image.network('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAPEAAADRCAMAAAAquaQNAAAA0lBMVEVBt4P///80SV7u7u7t7e319fX4+Pjy8vL7+/vz8/M3UWJLuohBt4Q3UGM0SV00R15CvIUus3szQFs4tX728fQ0RV4zQF1AvYTM6tz++fwvRVspP1fh6uX4/ftBs4Iqs3k/p36U1Lbe5OUbN09XvpE7hXF3x6I2YGXO49iu38g8knY4b2o+mHif07o4Z2dpxJvf8uk2WWOAzqtFXWro9e6ToaYhPVJxgovHzs+75NBTZnWtt7s7hnGFlpxqeog5d2s+oHq8xsqZ0LQ3a2fA39HY596o2YlsAAARjklEQVR4nN1deV/bOBC1IztOwCRxbI5QUqBAG7btQinb7bHbsl34/l9pLdnxqWOej5Cu/vIPk/E863ijebJk3fm7ULma7u3s7e2Isre3vipc7uWXHxlzhnZcRg6LL/mV7cZXzBOXHv+jKy7F/RG/GsaXH3dqpqSP2tnbGVxh7lt31n1gYWUyHVDLiz9cHDFz/3gxoT5hOgC9D+4t+zyEfjL77YCMePDpFcMRv7qgmp9MDn6bIc774blt2UsIsGUdfgAq+U8XRux+Pybb339z6EPO+8sYsf0FbNcP8wG51V18HiWIeUkQ86sUMb9MEfOS/OtnchUPBvMzzPXgi21bruteQr/yD9/uk106/jpyeRnGJb8aDvnVWPxxWLk/+gpU8VusTVuX0di1GItWpxBk6+U+uV1PLr6Ns1bM23Myktl5K+djM2NpK3eY9w2o4v2XWJs+XcWPihGz6HUI/XJ2NSc7dTxxSoiZFnH8R4AKDv6CqtgKX0cpYnaCDdfWbA8YvP52AcTu3y/Ilgc7GGArPGFrxNFPkKHeAQx18SpD7BhatQMwU1zF70DAPyP+LMuLy9jdxX48e0Ov5OPvI88d8TLmzxqKS37liauhcCC9P/pOr+Lp72AV7w497oEl3m4EMtTsDKnkz2R2AphpcnCGAQ4ek+aVIB5Hl2C7/gEw1EdyBPIRYKYfh5DH4U3kFhC7bAWGIS/pzXpw8c0mIbYRZpq+xBwOVmxcQhwzFGRg9he9XR8fC5gmxMw9plfx/K8ZxKjhXVRCPHZwhtqZkqs5ZihCP46ZiT5nasBM6aOsLBC6BgcvjKHKfDwqzpXW7AQx0wMGOLjOHpUjtnfBdv07efCavPhOiECQORPITOFu/qgC4iNw8Dqjx5rTmKFMiD9fkNv0ZH6GIQ6OpIjtm14ZyoR4B2EmsBffFB5lFWamKENZU3KtcIaKo8y4DPOII3UjLjFiiJkwN/3gqTBLt7LpqusuzsE5FMBQkxfMLTyqejl0gBkEmOqxwvNF4VFWkTLQBNAMSwBp2Mn9k454/wOY6rGWxUetI5AkLLgGSfmBPngNxOClikA+X5Df3XT+AA5b18mjmAyxjSWArNm/9DDk+KsGMZLq+Rds05e2DvHRKdZgXiKV/E/SqiWI/6EPW1M0oD7lzDQuIBYzU5dPUl17NOqVofZGdvooz84myfypyx36eDC/QlM9dhHgaFRkJ04TTyBD+aBEIWMn9w8gDzDB/LPCJ1YEGLNTHoGIYDfqUaKYfHoljUBefSKbaMBMUR7CV2OuJLzvmaEkiBFmmr7BJomWv3RMiGGJ4uwAkSi8GmKvZxGihrjcj3nn6lWi8Gr92PtKD1bBgNrnzFQFmLKTSCgmlOHBCSC6RBEzVDJCp4/yPDb6Rp8zTfdRZlp5SWKYJzFrfOys0+i9ShR743IEwrwBkOq5wuJLIUJUg51KBCKmdL1LFAXEiAjRINVTRCyPuQTiBhIFfdr46ZVXQOwhqZ75O5CZfkZMidgptOr4EpybQBJFzFDZ3IkB8vhk/w1Yxb7DdZ/1o7JWLZu0DhswFLlcfB5ljwLlcXDO9LiQTcXrfCwoo0+J4mvhUaAIAbW9mwL1ayMQcQ9nKHpdXXzLHgXJ42BXC1YRgNjFJQpERF+mMdcQEyEgj8K7SLZUTIkYZigLmPB9+jtF3LM8rkGcCgVsnSvnOtT7XkV0zhYxM5GnmpMDMNUTvo9SxAJVQf5Y605Zus1Zp1hREZ0uUQyOhUTRuwgxzKN3kdkra4ustiAHTQABEsVEZPn6ZCaR6sklLVPMlfbrviWKnd7mTFZ4YzdBfIsmgDCJ4hswbE1QZro1IS7044JUcocmgIDE5vEJvRMP5miq5y6JKir9OEXsZQtwhvkCnGS+vETnUEACCJgjDqYfQGaylmJZkZsvKyoAVPBxklRGRfSHA6Bh00sDeVy5WF0ZgaRpdDQB9C8QbJILKkJYl6V+iyE+6m+NKoC4gQihQ1zP7NkFARBlKCABRC3zK3BVz2tbtjguA2hVFj1XppMwQ+10DZiLEBDi4ESiUhcAWuU6LbITfyW4RNF1JcPMdB6t61TBTuoIRPTrEzQBhDAUoUwbpHrqC6eIMVeiQ6FrVB8AicJcJjAzfYlaInZQhpoBEoW57L8F15te2sQ6dtSIn5WhUGaKA+oqG2WI1/1Yz07xG/DQBFCHDDW/Aqv4dbTO1qrZSRuBiDo/wXqybwFrVA1lD0/1FASeJjGXQAxLFMh3ftoCihA+FyG6QNyrRKEr+7A8zhgBsbEfx2HIl6C3BJCm4OtNv0RiwaOhHyfz4XFc0g9WeEmmk/xKzJfRZV6HP/Y74GR8veliDcDLAYyrAOVKW7WVP6EM1UWz3seeaQVP0lZcY6cEsSYC4WUBMZTfCUPhIsSiirhhzCV+2+AripaAuTyOzRJPaspaw5hL3AcZyp+9a1vJqAgRvFfoTOlIlrdqw8iVXMb3YYmiXSWjIoS1mwIYVgHURq5MdVGzk7gPf0UBrJSolym6cIunetx0Ob6BnQgRSKLE3WAuzH60ibzmTUSIYXlZb4uYK0EMJ4AGLVK5jUSIrhHj22g0H7zwVI9NR0yIMtN+3qtEUSrTD2BAHS4TRKR+rEZYfQMRLlE0RIyLEJEMYbPMXumDUjS8bihRNBAhWB5FKiOQJojRBNBZQ8S4CKHYMKd5lOmmv0UTQICInpcmIgSMWKofx4jzfszEmHCLIfb9RvwEPYNvx5R6Xe/HTDKSGVSYioix2ABD4cy0oAPgKgyFj/NNPE4gZ5owFCxCWI58M5Vm+rFd+YTW8UCGghNADb7R80qIu4y5xG89lKFAiYJvx4SMW+Glx7pG7BRbNWO4RAFVMc5MTNqqHXWrVq98kW9NAzMUsEa10Td60g1zXM3KFx07laPM9O3Ba1T36BLFdAe0HZ4UvDZFmaZViqqtaXCJgl7JB+CXEMFPL++33cdca9v9SRQwM+06+m0ZO0IcPfa1RvXgDIsvuTzeELFstbGqH8f/2NN3frAIcRmVRh/ibDHn29KK8jKzDbPqF2uz0QQQUaKAt2O6zbzO67TGx5X7yq8GVBFI8vbQNaokiQIXISpe9xNzJb+9BxnqkCBRTHfQVM/9BhEv3oPt+t3cCBn+Ru/9ohliemav9PE/KFEcvjENXtPfsSUfMTOpNsyx9Zk903RSPvNcdP2dHxchQGZawF7Xd7kp16mSncTbgyUKfSU32Y7JtGmwvM4bRCAJ1+NfUeh7MmZNMJN5K+yOYq7UdqfbaBz8hgEWIkSviJ267S4lCvhLCH+p8EqhH49riJNNYMRvxXRSIE5mntn9fLuD5F/RBJBmoydQhPDFRoFyrwpeD/P74xxgU3aKhwSGrlFViui4COGpvOpSP661lwhOAKnCkP0zzNDpUZTPBjYTc6U9BPyK4lARXuPrTWtbb24KMXgWhW8NpKQMy+P3rRA37se8x3QiUTSRx7VeGfpxgrDJWD3yIga5KmcoXIRgHtN5JRurCwAlu9wQ+Zi3F1iikDDUwQOY6rmODF51sUpRGd2gEkX9K4r9txBgX4gQ1ANXuo0yE8StN3qCv4RYRZ0gduituqRvOPA2GhWGQkUIS5wJ4VS91qgu1fu0+bF861ZxAZ+WVNroCd6OKbgfUkRi3fxYWacZO5WUtpqm1UqimIIihB/8dPVeOWalrU0Ewsu4jUQBb8e061S92mzMJWx78FkUeXgNbxT4GG0B4hYMhYsQnnbde5l/wV1uym+rokmUs/8RxlD+LJMo4C1sV16tHnBNIu2MEt3JrsTZ6ogVlSjSnb4biRCa6N+teN2VfizhejgBJCQKfKPAJeRVPzFXahuUKJKvKFB5PLwGveoT8RCTKHy+0RMuQsBeaRAj60BkM09copjDX0IEjwuZV7h+rNnlxnAMVXFZEL7R0wF4WpF1Y0sX8Oi8wne5Kbdy9cwzbi/4VxQoM93aY0Pf62OVohoxylDWSzzVI/XqOWKuxDbKUGDhzCRdudBhZo9VR7Jahr48JqAJIKgIEYKmG5S9gne5QWae4GlJSAkvm8+Ha+urszptw07i7T72V8nBI1N5hbNT8tu2EYhQZUCGopfk+M8Gh0f3GHOJMQFOAFFLcN/Mqz4Ri1+hEgW1xMzUOeK2UWbaY9CzKKhlqVk7aerH9XUgSnai6E6VN+ChpyWRCv9Gr41XVXaScnmDCETcRxNAlMJFCM3ayeeLuZL7PTBUsNpqxKhEYS6CmTpF3GU/dpgDf0VhRHzC2nrl6He5cRV7RZTuq3dlQL/zMwL+act2sJB6TdwrImvFJqXNMUUgaTwHShSG4o/zRzXxyqnNlzuMuZJ/ja7BjZ60JfgStfBKE4F0iBg+LltXwpv0RKLuEcuyPIRVETLEbAUu89KU7GBqZatusCpCs8uNt96fSz5GjLx8JywvHyO8BXhakrqEdwuiV+N85JJ71WCXG5vOA+hXFGrE9x161cEqRbWKB55FoSoi1WPy6rljrlSHAr+ikJfkYGrTN0nbgRg+z09axJkQPSDuNMpcm0L3SpGWG43q4jTxqrRKkZljLjI78fu3rRnKP70VMZWOjymRYP8xVyqVoBJFrYTnyctrE4FsEPG4vUQRLtemmnvFZKNNX4jbShScmfpEPC7MnRxDj3EMiNf/2kqiEAdTj/N+bJo7MZVX9RVtNBUGWMC4vr94bDN4BY/DjrQhYJeb2thtXOOZ3+ebILWRKMKbqGCK4JVN8arPCESYQo/LLlbxE1Mg3taYa31cdsM5VFA4mPp5okxTq5YjdpbgzrV5WVZNmbxCWrVp5UuLNSb2dbPBK/hiq72irXzRne8k3kNHulPFlBM1C68v10lCbmqcPwrQneyKV12vUpRFINwU/BVFUsUrr26q6wikJ8SNJIr8YOqtRszkiPHDSIUIsQHE2C435n6cdT70tCS+HVMk6XxoP9atAykwW/5KpCvK5Wu3C19vy0zBCaDdZWaqWlGAV213uSGsUizfL5gCE0AxMylNGXfLf+6YKzG1wBjqsnpgwK8UZaZurE6ByOt0pTG1zZm9oimEoWJm0pnqxKtmu9xAM1NAogjv28yHaV51t0pRyk7CFPkrCv4lhN6Uqs5NXvWzSlEdGlIZShxMnbPRVsdcesSPNBG9ejD1syJ2WiGmfUWRHkzt0BFTvFIhFvPh5F5c1tEMvxwlb6S0R0Hy28L9objMTbkVUySJIri1R6mpNJzLTbGKVzbZq9zUGnHv7MSvIoJEkRz/aTS1nfpx3RRBogiXNFMVr7Yv5qIel50eTK01xX4lxLZhBRCXxzeEeCP92DOelhQc0U2180qs9VkPsPwySf3Z5Sxhup8Mvz/O/nW9n0yeu9SZMkgUN1EpIak1lXq1ZpjsvlvxWmpq1I9iLiNRrUTBRQidKUfl1bYo5jLEugRQeJ7pTNsSc1Fsm1KuuuOypW72ijhvH8ZW7Rjq2FE0Rc1pSaWDqQmmEFW7811uIJFXzlC+dTmCTTWZP7fd5Ua3n4zcVKQQ0YOVpqK69mpzEQg3JU8A5QdTA6Yyr7Y35hKmnqSIT6im2K+HWLbREz+YulEdt8lXF96WbpcbcV8z7uf35ab4o+sM5TsKNw2m6l7LNYmq153scqPSjzNTeXBckygSEUJq6hfUj2Wmqgx1UzWVN5+tjrnoiCsi+unT/x5x+bjs8LXmYOoN6cc9ZOhLporHZfv5wdQd9+PcVP37Y9OqmuouN/m/Gs+OkpsqfkURvLfdFqYq94knWsnbi4z5jDNPKonmEsVuW1N1Pt7cLjdEN7mpjKF4qqedqa2PuVJTaQIoP5h681FmZtupu6mapRjW3soQr02lEkV+MDU8DWvuFZOurx65lUv1zLP4r2OqqcUdX6Ma3i3iP0r/dUg2VbxPM7UJ/bhmKjmMNPl6vKWpZ93lhhaBCFM8AcR33taHDf+TmCs1tRteGt3casQMRXx0epTc3yrEOQx5hp7JELtSN+umznWmdIgBr5ySqfT+f/2rTXIpE1hRAAAAAElFTkSuQmCC',width: 75,height: 75),
                        Image.network('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTeawiqF62IDqVsWfctanAjOWLjQZQTJ19uiQIASYZt6KUIsICQ6Reo44BmkUh0XJh6Oqk&usqp=CAU',width: 75,height: 75),
                      ],
                    ),
                    ),
      ])));

   Widget contactView = Container(
    
    child: Card(
              child: Column(
                children: [
                  ListTile(
                    leading: Icon(Icons.phone,color: Colors.black),
                    title: const Text('ติดต่อ',style: TextStyle(fontSize: 25),)
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text('61160050@go.buu.ac.th', style: TextStyle(fontSize: 20),)
                    ),
      ])));

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'My resume',
      home:  Scaffold(
        appBar: AppBar(
          title: Text('My resume')),
          body: ListView(
            children: [
              profileView,
              educationView,
              experienceView,
              languageView,
              contactView
            ],
      )),
    );
  }
}